# Zehner Test - Alex



## Getting started

I used Shopify 2.0 dawn theme for test and implemented UI using the HTML, CSS, and Vanilla JS.

## What I did

I built the following sections in 4 hours.
- Pencil Banner section `sections/pencil-banner.liquid`
- Hero section `sections/hero.liquid`
- Icons section `sections/icon.liquid`
- Categories section `sections/categories.liquid`

## What I didn't complete

I didn't complete the following sections because of time being.
- Mega Menu
- Promo1 section
- Promo2 section
- Testimonials section
- Social section
- Pre-Footer / Footer section

## Challenge Approach

I used the following principles
- BEM styling
- Mobile first responsive styling

## My Suggestions to complete remaning sections

I need 8 more hours to complete remaining sections.

- Mega Menu: I would add a new section for mega menu and add blocks for each menu items.
- Promo1/2 sections: there is no problem. I would just implement the UI using HTML + CSS.
- Testimonials: For the slider, I would use slick.js or swiper.js library
- Social: I would add "featured image" and other image blocks.
- Pre-footer / footer: I would add a new section with some settings(title, description, CTA URL, and CTA Text ) and blocks (icon block) for pre-footer.
